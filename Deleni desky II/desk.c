#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/****OSETRENI VSTUPU****/
//velikost(vyska, sirka desky)
int whInput(int * p, int * q){
	if((scanf("%d %d", p, q) != 2 ) || (*p<=0) || (*q<=0)){
		printf("%s\n", "Nespravny vstup.");
		return 0;
	}
	return 1;
}

//maximalni plocha
int maxAreaInput(int * w){
	if((scanf("%d", w) != 1 ) || (*w<=0)){
		printf("%s\n", "Nespravny vstup.");
		return 0;
	}
	return 1;
}




/****REKURZIVNI REZANI DESKY****/
int recCut(int x, int y, int m, int *** mem){
	double d = (x<=y)?(y*1.0/x):(x*1.0/y);
	if( (x*y <= m) && (d<=2.0001) ){
		return 1;
	}
	
	if(mem[x-1]!=NULL && mem[x-1][y-1] != NULL && mem[x-1][y-1][0] > 0 ){
		return mem[x-1][y-1][0];
	}


	int min = INT_MAX; //minimalni pocet rezu..(prvni hodnota nabyva maxima)
	int tmp = 0; //bod rezu pro ziskani minimalniho poctu rezu
	
	for( int i = (x/2); i>=1; i--){
		int n = recCut(i,y,m, mem) + recCut(x-i,y,m,mem);
		if( min > n){
			min = n;
			tmp = i;
		}
	}
	
	for( int i = (y/2); i>=1; i--){
		int n = recCut(x,i,m,mem) + recCut(x,y-i,m,mem);
		if( min > n){
			min = n;
			tmp = (-1)*i;
		}
	}

	//Uozeni ziskanych minimalnich hodnot
	mem[x-1][y-1] = (int *)calloc(2, sizeof(int));
	mem[x-1][y-1][0] = min;
	mem[x-1][y-1][1] = tmp;
	return min;
}

/****REKURZIVNI VYPIS NEJLEPSI VARIANTY ROZREZANI DESKY****/
void printRects(int x, int y, int m, int *** mem, int level, int lr, int * li){
	li[level]=lr;
	for(int i = 2; i <= level; i++){
		if(li[i-1]==0){printf("| ");}
		else{printf("  ");}
	}

	if(lr==0){printf("+-");}  //Zacatek leve vetve
	else if(lr==1){printf("\\-");} //Zacatek prave vetve
	
	printf("[%d,%d]\n",x, y);

	double d = (x<=y)?(y*1.0/x):(x*1.0/y);
	if((x*y > m) || (d>2.0001) ){
		int dif = mem[x-1][y-1][1];
		if(dif<0){
			printRects(x,-dif, m, mem, level + 1, 0,li);
			printRects(x,y+dif, m, mem, level + 1, 1,li);
		}else{
			printRects(dif,y, m, mem, level + 1, 0,li);
			printRects(x-dif,y, m, mem, level + 1, 1,li);
		}
		
	}
}



/****UVOLNENI PAMETI****/
void freeMemory(int *** arr1, int * arr2, int a, int b){
	 for (int i=0; i<a; i++){
	 	for (int j=0; j<b; j++){
	 		if(arr1[i][j]!=NULL){
	 			free(arr1[i][j]);
	 		}
	 	}
	 	free(arr1[i]);
	 }
	 free(arr1);
	 free(arr2);
}



int main(void){
	int a, b; //vyska, sirka desky
	int maxArea; //maximalni obsah desky

	printf("%s\n", "Velikost:");
	if(whInput(&a, &b)){
		printf("%s\n", "Maximalni plocha:");
		if(maxAreaInput(&maxArea)){
			//alokace pole (pro zapamatovani hodnot rezu)
			int *** rects=(int ***)calloc(a,sizeof(int **));
		    for (int i=0; i<a; i++){
		        rects[i] = (int **)calloc(b, sizeof(int *)); 
		    }

		    //alokace pole (pro zapamatovani poctu carek na kazde urovni pri tisku)
			int * lines = (int *)malloc((a+b) * sizeof(int));


			//VYSTUP
			printf("Rezu: %d\n", recCut(a, b, maxArea, rects) - 1);
			printRects(a,b, maxArea,rects, 0, 2, lines);

			//UVOLNENI PAMETI
			freeMemory(rects, lines, a, b);
		}
	}
	
	return 0;
}
