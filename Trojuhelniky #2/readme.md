Úkolem je vytvořit program, který bude porovnávat dvojice trojúhelníků. Tato úloha je rozšířením jednodušší varianty zadání. Trojúhelníky v této úloze mohou být zadávané pomocí délek stran (SSS), dvojice stran a jimi sevřeného úhlu (SUS) a dvojice úhlů přilehlých k zadané straně (USU). Doporučujeme nejprve vyřešit jednodušší zadání a následně odladěné řešení rozšířit.

V rovině jsou zadané 2 trojúhelníky, každý trojúhelník je jednoznačně zadán jedním ze tří způsobů:

SSS - délkami svých stran,
SUS - délkami dvojice stran a velikostí úhlu jimi sevřeného (úhel je zadán ve stupních, pořadí na vstupu je strana úhel strana),
USU - velikostí dvojice úhlů a délkou přilehlé strany (úhly jsou zadané ve stupních, pořadí na vstupu je úhel strana úhel).
Program tato čísla přečte ze svého vstupu a rozhodne se pro jednu z následujících variant:
zda zadané vstupy tvoří trojúhelník,
zda jsou zadané trojúhelníky shodné,
zda jsou zadané trojúhelníky podobné, ale nejsou shodné, nebo
zda jsou zadané trojúhelníky zcela odlišné.
Pokud je vstup neplatný, program to musí detekovat a zobrazit chybové hlášení. Chybové hlášení zobrazujte na standardní výstup (ne na chybový výstup). Za chybu považujte:

na vstupu není žádný ze tří povolených způsobů zadání trojúhelníku (SSS/SUS/USU),
nečíselné zadání délek stran nebo velikosti úhlů,
délka strany je záporná nebo nulová,
úhel je menší nebo roven 0 stupňů, nebo větší nebo roven 180 stupňů,
chybějící vstupní údaj/údaje (strana/úhel).
Ukázka práce programu:
Trojuhelnik #1:
SSS 4 6.5 7
Trojuhelnik #2:
SSS 7 6.5 4
Trojuhelniky jsou shodne.
Trojuhelnik #1:
SSS 7 7 7
Trojuhelnik #2:
USU 60 13 60
Trojuhelniky nejsou shodne, ale jsou podobne.
Trojuhelnik #1:
  SSS
   4.5
 6       7
Trojuhelnik #2:
SUS 7 30 12
Trojuhelniky nejsou shodne ani podobne.
Trojuhelnik #1:
SSS 9.861 9.865 9.883
Trojuhelnik #2:
SSS 9861 9883 9865
Trojuhelniky nejsou shodne, ale jsou podobne.
Trojuhelnik #1:
USU 60 11 60
Trojuhelnik #2:
SUS 13 60 13
Trojuhelniky nejsou shodne, ale jsou podobne.
Trojuhelnik #1:
SUS 10 180 20
Nespravny vstup.
Trojuhelnik #1:
USU 120 20 90
Vstup netvori trojuhelnik.
Trojuhelnik #1:
S SS 20 30 30
Nespravny vstup.
Trojuhelnik #1:
SSS 20 30 50
Vstup netvori trojuhelnik.
Trojuhelnik #1:
USU -4 10 12
Nespravny vstup.
Trojuhelnik #1:
SUS 1 2 abcd
Nespravny vstup.
