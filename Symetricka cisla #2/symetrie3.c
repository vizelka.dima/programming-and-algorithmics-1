#include <stdio.h>
#include <math.h>
void preved(int x, int s){
	int y;
	do{
		y=x%s;
		if(s<10){printf("%d", y);}
		else{
			if(y<10){printf("%d", y);}
			else{
				switch(y){
					case 10: printf("a");break;
					case 11: printf("b");break;
					case 12: printf("c");break;
					case 13: printf("d");break;
					case 14: printf("e");break;
					case 15: printf("f");break;
					case 16: printf("g");break;
					case 17: printf("h");break;
					case 18: printf("i");break;
					case 19: printf("j");break;
					case 20: printf("k");break;
					case 21: printf("l");break;
					case 22: printf("m");break;
					case 23: printf("n");break;
					case 24: printf("o");break;
					case 25: printf("p");break;
					case 26: printf("q");break;
					case 27: printf("r");break;
					case 28: printf("s");break;
					case 29: printf("t");break;
					case 30: printf("u");break;
					case 31: printf("v");break;
					case 32: printf("w");break;
					case 33: printf("x");break;
					case 34: printf("y");break;
					case 35: printf("z");break;}
			}
		}
		
		x/=s;
	}while(x>0);
}
int main(void){
	long long int L, H, num; 
	int La[1000], Ha[1000], NUMa[1000];
	int soustava;
	int count;
	char p;
	int nl, nh, k, m, t, a, n;
	bool poly;
	printf("%s\n", "Vstupni intervaly:");
	while(1){
		int skan1, skan2, skan3, skan4;
		skan1 = scanf(" %c", &p);
		skan2 = scanf("%d", &soustava);
		skan3= scanf("%lld", &L);
		skan4= scanf("%lld", &H);
		if((skan1 == EOF)&&(skan2==EOF)&&(skan3==EOF)&&(skan4==EOF)){
			break;
		}
		else if((skan3==EOF)||(skan4==EOF)){
		    printf( "Nespravny vstup.\n" );
			break;
		}
		else if((skan1!=1)||(skan2!=1)||(skan3!=1)||(skan4!=1)||((p!='l')&&(p!='c'))||
		(L<0)||(L>H)||(soustava<2)||(soustava>36)){
			char c;
			printf( "Nespravny vstup.\n" );
			do { c = getchar(); } while ( c != '\n' );
			break;
		}
		if((p=='c')&&(H>7000)){
			count=0;
			if(L==0){
				count++;
				L=1;
			}
			if(H!=0){
				nl=floor(log(L)/log(soustava));
				nh=floor(log(H)/log(soustava));
				
				//LOW
				k = round(nl/2);
				m = floor(nl/2);
				if(nl%2==1){
					k = round((nl+1)/2);
				}
				t = k;

				
				//PREVOD L DO SOUSTAVY
				a=0;
				long long int memL=L;
				while(memL>0){
					La[a]=(memL%soustava);
					memL/=soustava;
					a++;
				}

				//PREVOD H DO SOUSTAVY
				a=0;
				long long int memH=H;
				while(memH>0){
					Ha[a]=(memH%soustava);
					memH/=soustava;
					a++;
				}

				//TEST L na rovnost , levo == pravo?

				while( (La[k]==La[m]) && (m>=0)){
					k++;
					m--;
				}
				if(m==(-1)){
					count++;
				}
				
				else if((La[k]>La[m]) && (m>=0)){
					count++;
				}
				
				
				for(int y = t; y<=nl; y++){
					count+=( (soustava - (La[y] + 1) )*((long long int)round(pow(soustava, y-t)))  );
				}
			

				//HIGH
				k = round(nh/2);
				m = floor(nh/2);
				if(nh%2==1){
					k = round((nh+1)/2);
				}
				t = k;
			
				while( (Ha[k]==Ha[m]) && (m>=0)){
					k++;
					m--;
				}

				if((Ha[k]>Ha[m]) && (m>=0)){
					count--;
				}
				
				for(int y = t; y<=nh; y++){
					count-=( (soustava - (Ha[y] + 1) )*((long long int)round(pow(soustava, y-t))) );
				}

					
				//DODATEK
				
				if(nl!=nh){
					for(int f = nl+1; f<=nh; f++){
						count+=(soustava-1)*(long long int)round(pow(soustava,(round((f)/2))));
					}
				}
			}
			
			printf("Celkem: %d\n", count);
			
		

		}
		else if((p=='l')||((p=='c')&&(H<=7000))){
			
			count=0;
			if(L==0){
				count=1;
				if(p=='l'){
					printf("0 = 0 (%d)\n", soustava);
				}
			}

			for(long long int i=L; i<=H; i++){
				num=i;
				
				if(num%soustava!=0){
					n=floor(log(num)/log(soustava));
					t=0;
					poly=false;

					//PREVOD num DO SOUSTAVY
					a=0;
					long long int memL=i;
					while(memL>0){
						NUMa[a]=(memL%soustava);
						memL/=soustava;
						a++;
					}
			
					do{
						if(NUMa[n]==NUMa[t]){poly=true;}
						else{poly=false;}
						t++;
						n--;
					       
					}while(poly && n>=t);
	
					if(poly){
						count++;
						if(p=='l'){
							printf("%lld = ", i);
							preved(i, soustava);
							printf(" (%d)\n", soustava);
						}
					}
				}
				 
			}
			if(p=='c'){
					printf("Celkem: %d\n", count);
				}
		}

	}
	return 0;
}

