#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
void seradx(double ar[], int f, int l){
    if(f<l){
        //prvni prvek
        double pivot = ar[f];
        int a = f, b = l;
        //cyklus dokud prvky mensi nez pivot nebudou mit mensi index nez prvky vetsi(nebo rovny) nez pivot
        while(a<=b){
        	//HLEDAM PRVEK(od nulteho prvku), ktery je vetsi nebo roven pivotu. 
            while(ar[a]<(pivot)){a+=4;}

            //HLEDAM PRVEK(od konce), ktery je mensi nebo roven pivotu. 
            while(ar[b]>(pivot)){b-=4;}

            //IF prvek vetsi [a] nez pivot ma mensi index <<<< nez prvek mensi [b] nez pivot --> vymena
            if(a<=b){
                double tmp0 = ar[a];
                double tmp1 = ar[a+1];
                double tmp2 = ar[a+2];
                double tmp3 = ar[a+3];
                ar[a] = ar[b];
                ar[a+1] = ar[b+1];
                ar[a+2] = ar[b+2];
                ar[a+3] = ar[b+3];
             
                ar[b] = tmp0;
                ar[b+1] = tmp1;
                ar[b+2] = tmp2;
                ar[b+3] = tmp3;
                a+=4;
                b-=4;
            }
 

        } 
        seradx(ar, f, b); //leva cast
        seradx(ar, a, l); //prava cast
    }
}

void seradd(double ar[], int f, int l){
    if(f<l){
        //prvni prvek
        double pivot = ar[f];
        int a = f, b = l;
        //cyklus dokud prvky mensi nez pivot nebudou mit mensi index nez prvky vetsi(nebo rovny) nez pivot
        while(a<=b){
        	//HLEDAM PRVEK(od nulteho prvku), ktery je vetsi nebo roven pivotu. 
            while(ar[a]<(pivot)){a+=5;}

            //HLEDAM PRVEK(od konce), ktery je mensi nebo roven pivotu. 
            while(ar[b]>(pivot)){b-=5;}

            //IF prvek vetsi [a] nez pivot ma mensi index <<<< nez prvek mensi [b] nez pivot --> vymena
            if(a<=b){
                double tmp0 = ar[a];
                double tmp1 = ar[a+1];
                double tmp2 = ar[a+2];
                double tmp3 = ar[a+3];
                double tmp4 = ar[a+4];

                ar[a] = ar[b];
                ar[a+1] = ar[b+1];
                ar[a+2] = ar[b+2];
                ar[a+3] = ar[b+3];
             	ar[a+4] = ar[b+4];

                ar[b] = tmp0;
                ar[b+1] = tmp1;
                ar[b+2] = tmp2;
                ar[b+3] = tmp3;
                ar[b+4] = tmp4;

                a+=5;
                b-=5;
            }
 

        } 
        seradd(ar, f, b); //leva cast
        seradd(ar, a, l); //prava cast
    }
    
}
   

int main(void){
	printf("%s\n", "Zadejte lety:");
	//Let, x y nazev

	
	int max = 20;
	int maxName = 1000;

	char *names = (char *)malloc(sizeof(char) * max * maxName); 

	double *pos = (double *)malloc(sizeof(double) * 4 * max); 

	size_t len=0, l=0;
	char *line = NULL; 
	int t = 0;
	int tmp = 0;
	int notok = 0;
	int ll = 0;
    char space;
	while(1){
		l += ll - tmp;
	    if(t>=(4*max)){
            max *= 3;
            names = (char *)realloc(names, sizeof(char) * max * maxName); 
            pos = (double *)realloc(pos, sizeof(double) * 4 * max);
         
        }
		ll = getline(&line, &len, stdin);
		if(maxName < ll){
		    maxName = ll;
		     names = (char *)realloc(names, sizeof(char) * max * maxName); 
		    }
		if(ll==EOF){
		    break;
		}
		
		if(sscanf(line, " [ %lf , %lf ]%c %n", pos+t, pos+t+1, &space,  &tmp)!=3){
			notok = 1;
			break;
		}
     
		*(pos + t + 2)=l; //tocislo az PS
		*(pos + t + 3)=l+ll-tmp; //PS: MENSI NEZ
		for(int k = 0; k < ll-tmp; k++){
			*(names+l+k)=*(line+tmp+k);
		}

		t+=4;
	    
	} 

	free(line);
  


	if((t<=4)||(notok == 1)){
		printf("%s\n", "Nespravny vstup.");
		free(pos);
		free(names);
		return 0;
	}
	else{ 
		seradx(pos, 0, t-4);
		int p = 0;
		
	    double *dis = (double *)malloc(sizeof(double) * 5 * max * max); 
	    for(int q=0; q<t-4; q+=4){
	        int pp=4;
			double d = sqrt( pow( fabs(*(pos+q) - *(pos+q+pp) ) , 2) + pow( fabs(*(pos+q+1) - *(pos+q+pp+1) ) , 2) ) ;
			
			*(dis + p) = d;
			*(dis + p + 1) = *(pos+q+2);
		    *(dis + p + 2) = *(pos+q+3);
		    *(dis + p + 3) = *(pos+q+pp+2);
		    *(dis + p + 4) = *(pos+q+pp+3);
		      
				while( (fabs(*(pos+q) - *(pos+q+pp+4) ) <= d+13)&&((q+pp)<(t-4)) ){
				       pp+=4;
				     	 p+=5;
				        *(dis + p) = sqrt( pow( fabs(*(pos+q) - *(pos+q+pp) ) , 2) + pow( fabs(*(pos+q+1) - *(pos+q+pp+1) ) , 2) );
        			    *(dis + p + 1) = *(pos+q+2);
        			    *(dis + p + 2) = *(pos+q+3);
        			    *(dis + p + 3) = *(pos+q+pp+2);
        			    *(dis + p + 4) = *(pos+q+pp+3);
			        
			}
			p+=5;
		}
	
		free(pos);
		
	    seradd(dis, 0, p-5);
       


        double *minNames =  (double *)malloc(sizeof(double) * p);
       
  
		
		
		int loc = 5;
		double min = dis[0];
		int count=0;
		minNames[0] = dis[1];
        minNames[1] = dis[2];
        minNames[2] = dis[3];
        minNames[3] = dis[4];
		while( (loc<p) && ( fabs(min - dis[loc]) <= DBL_EPSILON*1000*fabs(min+dis[loc])) ){
			count+=4;
			minNames[count] = dis[loc+1];
            minNames[count+1] = dis[loc+2];
            minNames[count+2] = dis[loc+3];
            minNames[count+3] = dis[loc+4];
			loc+=5;
		}

        free(dis); 
               
	  	printf("Nejmensi vzdalenost: %f\n", min);

		for (int i = 0; i <= count; i+=4)
			{
				for(int j = *(minNames+i); j < *(minNames+i+1) - 1; j++){
					printf("%c", *(names+j));
				}	 
				printf (" - ");
				
				for(int f = *(minNames+i+2); f < *(minNames+i+3) - 1 ; f++){
					printf("%c", *(names+f));
			    }	
			    printf ("\n");
			}
					  
	   
	    free(minNames);
	    free(names);
	    return 0;	
	}

	free(pos);
	free(names);
	return 0;
}