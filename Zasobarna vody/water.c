#include <stdio.h>
#include <math.h>

void serad(int ar[], int f, int l){
	//dokud v leve casti  b neprekroci nulty index
	//nebo a neprekroci posledni index
    if(f<l){
    	//Pivot = prostredni prvek
        //int c = (f+l)/2;

        //posledni prvek
        int pivot = ar[l];
        int a = f, b = l;
        //cyklus dokud prvky mensi nez pivot nebudou mit mensi index nez prvky vetsi(nebo rovny) nez pivot
        while(a<=b){
        	//HLEDAM PRVEK(od nulteho prvku), ktery je vetsi nebo roven pivotu. 
            while(ar[a]<pivot){a++;}

            //HLEDAM PRVEK(od konce), ktery je mensi nebo roven pivotu. 
            while(ar[b]>pivot){b--;}

            //IF prvek vetsi [a] nez pivot ma mensi index <<<< nez prvek mensi [b] nez pivot --> vymena
            if(a<=b){
                int tmp = ar[a];
                ar[a] = ar[b];
                a++;
                ar[b] = tmp;
                b--;
            }
 

        } 
        //a prekroci b
        //Rekurentne seradit levou a pravou cast
        serad(ar, f, b); //leva cast
        serad(ar, a, l); //prava cast
    }
}

int main(void){	
	int n;
	int end=0;
	int skan1, skan2;
	double Lw = 0;

	//POCET ZASOBNIKU
	printf("Zadejte pocet nadrzi:\n");
	if( (scanf("%d", &n)!=1)||
		(n<=0)||
	    (n>200000)
	   ){
		printf( "Nespravny vstup.\n" );
	}
	else{
		int alt, w, h, d; //zasobnik // [alt, w, h, d]
		int Vv, Vn = 0;
		int z[200000][3];
		int zone[500000];
		int S[200000];
		int AVz[200000], AHz[200000];
		int tmp=0;
		printf("Zadejte parametry nadrzi:\n");
		for(int i = 0; i<n; i++){
			skan1 = scanf("%d %d %d %d", &alt, &h, &w, &d);
			if(skan1 == EOF){printf( "Nespravny vstup.\n" );end=1;break;}
			if((skan1!=4)||(w<=0)||(h<=0)||(d<=0)){
			    printf( "Nespravny vstup.\n" );
			    end = 1;
				break;
			}
			z[i][0] = alt;
			z[i][1] = h + alt;
			z[i][2] = w * d;


			zone[i] = alt;
			zone[2*(n)-1-i] = h+alt;
			Vn += (z[i][1]-z[i][0]) * z[i][2];
		}

		//HRANICE ZON
			//SERADIT MIN->MAX 
			/*int tmp;
			for(int k = 0; k<2*n; k++){
				for(int l = k+1; l<2*n; l++){
					if(zone[l]<zone[k]){
						tmp = zone[k];
						zone[k] = zone[l];
						zone[l] = tmp; 
					}
				}
			}*/

			//quicksort pro rychlejsi serazeni pole #BONUS
			serad(zone, 0, (2*n) - 1);


			//ZBAVIT SE DUPLICIT 
			int j = 0;
			for (int i = 0; i < 2*n; i++){ 
		        if (zone[i] != zone[i+1]){ 
		            zone[j] = zone[i];
		            j++;
		        }
		     }
            
		     //zobraz hranice zon
	    	//for (int i = 0; i < j; i++) {printf("%d ",  zone[i]);} printf("\n");

		if(end == 0){
			printf( "Zadejte objem vody:\n" );
			while(1){
				skan2 = scanf("%d", &Vv);
				if(skan2 == EOF){
					break;
				}
				if((skan2!=1)||(Vv<0)){
				    printf( "Nespravny vstup.\n" );
					break;
				}

				if(Vv==0){printf( "Prazdne.\n" );}
				else if(Vv > Vn){printf( "Pretece.\n" );}
				else{	
					//PROJDE KAZDY PRVEK A SPOCITA PLOCHU ZONY DOKUD SE OBJEM ZBYTKU VODY NEROVNA 0
					int x = 0;
				 
					Lw = zone[0];
					int znamObjem = 0;
					//Zjistim jakou zonu znam -- Dokud objem zony neni vetsi nez objem vody 
					if((tmp=0)&&(AVz[tmp]<=Vv)){x=tmp;}
					while((AVz[x]!=0) &&(Vv >= AVz[x])){
							znamObjem = 1;
							x++;	
						}
					//Odectu objem, ktery mam uz vypocitany
					if(znamObjem == 1){
						Vv -= AVz[x-1];
						Lw += AHz[x-1];
					}
					//dokud mam nejaky zbytek vody
					while(Vv > 0){
						//SPOCITEJ PLOCHU ZONY x - Sx

						//Pokud neznam plochu
						if(S[x]==0){
							//prochazi pouze pokud aktualni S[x] se rovna 0 + ukladat objem a pak if (Vv vetsi nez znamy objem...)
							for(int y = 0; y<n; y++){
								if((z[y][0]<zone[x+1]) && (z[y][1]>zone[x])){
									//printf("%d < %d a zaroven %d > %d\n", z[y][0] , zone[x+1], z[y][1], zone[x]);
									S[x] += z[y][2];
									//printf("Sx == %d\n", S[x]);
								
								}
							}
						}
						//if zbyly objem vody je vetsi nebo roven objemu zony
						int Hz = (zone[x+1] - zone[x]);
						int Vz = S[x] * Hz;
						if(x!=0){AVz[x] = AVz[x-1] + Vz; AHz[x] = AHz[x-1] + Hz;}else{AHz[x]=Hz;}
					//	printf("Hz == %d\n", Hz);
					//	printf("Vz == %d\n", Vz);
					//	printf("Vv == %d\n", Vv);
						
						//If V vody vetsi nez V zasovniku --->> Snizuju zbytek vody , zvysuje Water Level o vysku zony
						
							Vv -= Vz;
							Lw += Hz;
						

						//pokud ne --> dopocitam vysku zbytku vody v dane zone ---> pak prictu k Water Level
						
						x++;
					}
					Lw += 1.0*(Vv) / S[x-1];
					tmp = x-1;
					printf("h = %lf\n", Lw);
				}
			}
		}
	}
return 0;
}

